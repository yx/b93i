/* b93i - yx
based on Befunge-93 standard here: https://github.com/catseye/Befunge-93/blob/master/doc/Befunge-93.markdown
enjoy!
*/

#include <stdio.h>
#include <stdlib.h>

// globals for program
static char program[25][80] = { 0 }; // zero initialize

// stack information
static unsigned char sp = 0;
static signed long int stack[1 << (sizeof(sp) * 8)];

// stack functions
inline void push(signed long int x) {
	if (sp + 1 == 0) {
		printf("error: stack overflow\n"); // unlikely to reach this
		exit(-3);
	}
	stack[sp++] = x;
}

inline signed long int pop() {
	if (sp == 0) return 0; // underflows return 0 according to the standard
	return stack[--sp];
}

// program counter information
static signed char pc[2] = {0, 0}; // pc can work in two directions {y, x}

enum Direction {
	Right,
	Down,
	Left,
	Up
};

inline void movepc() {
	pc[0] += (pcdir == Down) - (pcdir == Up);
	if (pc[0] < 0) pc[0] = 24; if (pc[0] > 24) pc[0] = 0;
	pc[1] += (pcdir == Right) - (pcdir == Left);
	if (pc[1] < 0) pc[1] = 79; if (pc[1] > 79) pc[1] = 0;
}

// string mode?
enum Mode {
	Standard,
	String
};

static unsigned char mode = Standard;

static pcdir = Right;

int main(int argc, char *argv[]) {
	// check for proper usage
	if (argc != 2) {
		printf("usage: %s [program.b93]\n", argv[0]);
		return -1;
	}
	printf("Befunge-93 Interpreter\nloading %s\n", argv[1]);
	// input file exists?
	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		printf("error: could not open %s\n", argv[1]);
		return -2;
	}

	// load program
	char next;
	for (int y = 0; y < 25; y++) {
		for (int x = 0; x < 80; x++) {
			next = fgetc(fp);
			if (next == EOF || next == '\n') {
				for (int i = x; i < 80; i++) program[y][i] = ' '; // fill spaces
				break;
			} // some sort of error, likely the program is empty here
			program[y][x] = next;
		}
		// note that ^ will just ignore program instructions that exceed size limits
	}
	// clean up
	fclose(fp);

	signed long int temp, temp2; // switch statements are stupid
	// main loop time, statics already initialized
	for (;;) {
		// handle string mode
		if (mode == String) {
			char c = program[pc[0]][pc[1]];
			movepc();
			if (c == '"') {
				mode = Standard;
			}
			else {
				push(c); // place character on stack
				continue; // skip normal execution while making strings
			}
		}

		// handle standard mode
		char instruction = program[pc[0]][pc[1]];
		switch (instruction) {
		// program counter manipulation
		case '<': pcdir = Left; break;
		case '>': pcdir = Right; break;
		case '^': pcdir = Up; break;
		case 'v': pcdir = Down; break;
		case '?': pcdir = rand() % (Up - Right); break; // random pcdir
		case '#': movepc(); break; // bridge, second movement done at loop end

		// arithmetic
		case '+': push(pop() + pop()); break;
		case '-': push(-pop() + pop()); break;
		case '*': push(pop() * pop()); break;
		case '/': push(1.0 / pop() * pop()); break;
		case '%': // couldn't avoid temp for this one
			temp = pop();
			push(pop() % temp);
			break;
		
		// comparision and logic
		case '!': push(pop() == 0); break;
		case '`': push(pop() < pop()); break;

		// conditional control flow
		case '_': // horizontal if
			if (pop() != 0) pcdir = Left;
			else pcdir = Right;
			break;
		case '|': // vertical if
			if (pop() != 0) pcdir = Up;
			else pcdir = Down;
			break;
		case '@': // end program
			printf("\n"); // ensure output is rendered
			return 0;
			break; // no cleanup needed: everything is stack

		// stack manipulation
		case ':': // basically dup
			temp = pop();
			push(temp);
			push(temp);
			break;
		case '\\': // swap
			temp = pop();
			temp2 = pop();
			push(temp2);
			push(temp);
			break;
		case '$': pop(); break;

		// string mode
		case '"':
			mode = String;
			break;

		// input
		case '&': // get integer from stdin
			scanf("%i", &temp);
			push(temp);			
			break;
		case '~': push(getchar()); break;

		// program modification
		case 'g': // get instruction
			push(program[pop()][pop()]); break;
		case 'p': // put instruction
			temp = pop();
			program[pop()][pop()] = temp;
			break;

		// number pushing
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			push(instruction - '0'); break;

		// output
		case ',': printf("%c", pop()); break;
		case '.': printf("%i", pop()); break;
		}		

		//printf("%i, %i, %i\n", pc[0], pc[1], pcdir);
		movepc();
	}
}
