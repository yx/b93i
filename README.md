# b93i - a Befunge-93 interpreter
## a truly groundbreaking language

Befunge is a 2-dimensional stack-based programming language. Programs are executed in any cardinal direction on a 80x25 grid
